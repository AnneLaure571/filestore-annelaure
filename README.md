# filestore-mediatheque

# Miage.20 FileStore

Sources for project miage.20

### Start and configure keycloak

In order to authenticate users we will use an already existing IdP : keycloak (https://www.keycloak.org/)

Use docker to run an instance and map a local folder to this container, allowing to persist data between restarts

```bash
docker pull jboss/keycloak:9.0.0
docker run --name keycloak -p 8080:8080 -v /var/keycloak:/opt/jboss/keycloak/standalone/data jboss/keycloak:9.0.0
docker exec keycloak keycloak/bin/add-user-keycloak.sh -u admin -p admin123
docker restart keycloak
```

Try then to access http://localhost:8080/auth and configure realm :

- Create a new realm 'Filestore' 
- Add a client named filestore (Direct Access Grant has to be enabled for the client)


### Start the application : 

Compile the projet, initilise db with default users (admin/admin, user/user) and start the application.

```bash
mvn clean package -DskipTests
mvn liquibase:migrate
java -cp ~/.m2/repository/com/h2database/h2/1.4.196/h2-1.4.196.jar org.h2.tools.RunScript -url jdbc:h2:~/.filestore/filestore -user sa -password sa -script ./src/main/resources/scripts/users.sql -showResults
java -jar ./target/jayblanc-1.0.0-SNAPSHOT-thorntail.jar -Dswarm.port.offset=100
```

Open browser to : http://localhost:8180/fs/api/files

### Start another instance of the application (neighbourhood testing) : 

```bash
mvn liquibase:migrate -P 2
java -cp ~/.m2/repository/com/h2database/h2/1.4.196/h2-1.4.196.jar org.h2.tools.RunScript -url jdbc:h2:~/.filestore/filestore-2 -user sa -password sa -script ./src/main/resources/scripts/users.sql -showResults
java -jar ./target/jayblanc-1.0.0-SNAPSHOT-thorntail.jar -Dswarm.port.offset=200 -Dfilestore.instance.id=2 -Dfilestore.instance.name=Karmic_Wonderland  -Dswarm.datasources.data-sources.fsDS.connection-url=jdbc:h2:~/.filestore/filestore-2;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```

Open browser to : http://localhost:8280/fs/api/files  
or : http://localhost:8180/api/files/42/content

### Remove all db and files (fresh install) :

```bash
rm -Rf ~/.filestore/*
```

### Start dockerized application

Set the persistence layer to auto create database avoiding calling liquibase inside the container.

It is configured in the file: resources/META-INF/persistence.xml 

```xml
<!--<property name="javax.persistence.schema-generation.database.action" value="validate"/>-->
<property name="javax.persistence.schema-generation.database.action" value="create"/>            
```

Build the docker image using maven plugin.

```bash
mvn clean package dockerfile:build -DskipTests
```

Create local folders to host database and all persistent container data, allowing restart without data loss

```bash
sudo mkdir -P /var/filestores/sheldon
sudo chmod -R 777 /var/filestores
```

Run a specific instance of the filestore

```bash
sudo docker run -d -t -i -e FS_AUTH_URI='http://localhost:8080' \
-e FS_OWNER='sheldon' \ 
-e FS_ID='sheldon-fs' \
-e FS_NAME='Sheldon Cooper' \
-e FS_FQDN='http://sheldon.localhost' \
-v /var/filestores/sheldon:/opt/jboss/filestore \
--name sheldon-fs
dokku/filestore-1.0.0-SNAPSHOT
```
# Miage.20 FileStore - dokku

Comment mettre en place le Dokku.

## Connection VPN aux serveurs de l'Université de Lorraine

### Tutoriel de création de la connexion VPN

Suivez le tutoriel disponible ici pour se connecter au réseau de l'UL :
https://wikidocs.univ-lorraine.fr/pages/viewpage.action?spaceKey=infra&title=Documentation+officielle+VPN
Cette étape est obligatoire pour pouvoir se connecter au serveur Dokku de Jayblanc.

### Tester la connexion VPN

Pour vérifier que la connexion VPN au réseau de l'UL est fonctionnelle, il vous suffit de pinguer le serveur de Jayblanc :
```bash
ping http://jayblanc.filestore.jayblanc.fr/
```

## Mettre en place la communication ssh

### Créer un fichier de configuration ssh

Vous devez déjà avoir au préalable générer votre clé ssh et l'avoir transmise à Jayblanc.

#### Création du fichier de configuration sous Windows

Sur votre Windows, dans le dossier "C:\Users\nomUser\.ssh" où "nomUser" désigne votre nom d'utilisateur sur votre machine, créer un fichier "config".
Mettre dedans :
```bash
Host dokku.plg
    Hostname filestore.jayblanc.fr
    User dokku
    IdentityFile C:\Users\nomUser\.ssh.ssh\id_rsa
    IdentitiesOnly yes
```
où "nomUser" désigne votre nom d'utilisateur

#### Création du fichier de configuration sous Linux

Sur votre Linux, dans le dossier "~/.ssh/", créer un fichier "config".
Mettre dedans :
```bash
Host dokku.plg
    Hostname filestore.jayblanc.fr
    User dokku
    IdentityFile ~/.ssh/id_rsa
    IdentitiesOnly yes
```
### Tester la configuration ssh

Sur Windows comme sur Linux, dans un terminal, tapez la commande :
```bash
ssh dokku.plg apps:list
```
Cela devrait retourner quelque chose du genre :
```bash
=====> My Apps
auth
filestore-alcharles
filestore-aremiatte
filestore-bhubert
filestore-boudart
filestore-jayblanc
filestore-ppopinot
jayblanc
jenkins
manager
```

## Préparer votre projet au déploiement sur Dokku

### Création et/ou modification du fichier Procfile

A la racine de votre projet, créer et/ou modifier le fichier "Procfile" contenant :
```bash
web: java $JAVA_OPTS -Dthorntail.http.port=$PORT -Dthorntail.bind.address=`hostname -i` -Dthorntail.port.offset=0 -jar ./target/filestore-alcharles-1.0.0-SNAPSHOT-thorntail.jar
```

### Création et/ou modification du fichier system.properties

A la racine de votre projet, créer et/ou modifier le fichier "system.properties" contenant :
```bash
java.runtime.version=11
```

### Modification du fichier keycloack.json

Dans le dossier src/main/resources, modifier le fichier "keycloack.json" de la sorte :
```bash
{
  "realm": "filestore",
  "public-client": true,
  "auth-server-url": "http://filestore.jayblanc.fr/auth/",
  "ssl-required": "external",
  "resource": "filestore"
}
```
A l'issue, n'oubliez pas de faire un commit pour mettre à jour votre git local !

## Déploiement sur Dokku

### Création de votre App sur Dokku

Dans votre terminal, tapez la commande suivante :
```bash
dokku apps:create filestore-alcharles
```
N'oubliez pas de remplacer "filestore-alcharles" par le nom de votre FileStore comme sur le GitLab.

### Création du remote de votre App sur Dokku

En vous placant à la racine de votre projet, tapez la commande suivante dans votre terminal :
```bash
git remote add dokku dokku@filestore.jayblanc.fr:filestore-alcharles
```
N'oubliez pas de remplacer "filestore-alcharles" par le nom de votre FileStore.

### Déploiement de votre App sur Dokku

En vous placant à la racine de votre projet, tapez la commande suivante dans votre terminal :
```bash
git push dokku master
```
Si tout se passe bien, cette commande va vous retourner à la fin l'URL pour accéder à votre FileStore sur le serveur de Jayblanc.
Pour ma part, il s'agit de : http://filestore-alcharles.filestore.jayblanc.fr

Si vous avez des problèmes, vous pouvez détruire votre App avec la commande suivante :

```bash
ssh dokku.plg apps:destroy filestore-alcharles
```

FAITES ATTENTION, ne mettez pas dans le .gitignore, votre Procfile ou system.properties (et n'oubliez pas de add + commit avant, pas nécessaire de push).

si vous avez une erreur "404 found", pensez à vider votre cache.

lien : http://filestore-alcharles.filestore.jayblanc.fr/api/videos/42/content

## Initialisation de votre FileStore sur le serveur de Jayblanc

Lancez depuis votre navigateur l'URL récupéré précedemment.
Cela devrait normalement vous rediriger sur le serveur Keycloak installé par Jayblanc sur son serveur.
A partir d'ici, cliquer sur le bouton "Register".
Renseignez les champs en fonction de vos informations, mais ATTENTION ! Renseignez le même pseudo que celui que vous avez défini dans votre projet dans le fichier "project-defaults.yml" dans le champs "owner", sinon vous ne pourrez pas vous connecter à votre filestore !
Enfin, une fois l'inscription terminée, vous devriez être redirigé sur votre filestore, et voilà !

## Comment afficher les logs du Dokku

Si vous avez besoin d'afficher vos logs :
```bash
ssh dokku.plg logs filestore-alcharles -n 20000
```
N'oubliez pas de remplacer "filestore-alcharles" par le nom de votre FileStore.


