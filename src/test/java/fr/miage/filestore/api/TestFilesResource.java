package fr.miage.filestore.api;

import fr.miage.filestore.file.entity.FileItem;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.undertow.WARArchive;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.util.logging.Logger;

import static org.junit.Assert.assertTrue;

//@RunWith(Arquillian.class)
public class TestFilesResource {

    private static final Logger LOGGER = Logger.getLogger(TestFilesResource.class.getName());

    /*
    @Deployment
    public static Archive createDeployment() throws Exception {
        WARArchive archive = ShrinkWrap.create(WARArchive.class);
        archive.addPackage("fr.miage.filestore.api");
        archive.addPackage("fr.miage.filestore.api.dto");
        archive.addPackage("fr.miage.filestore.api.filter");
        archive.addPackage("fr.miage.filestore.api.mapper");
        archive.addPackage("fr.miage.filestore.api.resources");
        archive.addPackage("fr.miage.filestore.api.template");
        archive.addPackage("fr.miage.filestore.api.validation");
        archive.addPackage("fr.miage.filestore.config");
        archive.addPackage("fr.miage.filestore.file");
        archive.addPackage("fr.miage.filestore.file.entity");
        archive.addPackage("fr.miage.filestore.file.filter");
        archive.addPackage("fr.miage.filestore.notification");
        archive.addPackage("fr.miage.filestore.notification.entity");
        archive.addPackage("fr.miage.filestore.neighbourhood");
        archive.addPackage("fr.miage.filestore.neighbourhood.entity");
        archive.addPackage("fr.miage.filestore.auth");
        archive.addPackage("fr.miage.filestore.auth.entity");
        archive.addPackage("fr.miage.filestore.store");
        archive.addAsWebInfResource("jboss-web.xml");
        archive.addAsResource("META-INF/beans.xml");
        archive.addAsResource("META-INF/persistence.xml");
        archive.addAsResource("templates/files.ftl");
        archive.addAsResource("templates/status.ftl");
        archive.addAsResource("liquibase/migration/db-changelog.xml");
        archive.addAsResource("project-defaults.yml");
        File[] files =
                Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies()
                        .resolve("junit:junit:4.12").withTransitivity().asFile();
        archive.addAsLibraries(files);

        return archive;
    }
    */

    /*
    @Test
    public void testGetFile() {
        Client client = ClientBuilder.newClient();
        //ItemCollection<FileItem> files = client.target("http://localhost:8080/fs/api/files").request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType <ItemCollection<FileItem>> () {});
        FileItem file = client.target("http://localhost:8181/api/files/42").request(MediaType.APPLICATION_JSON_TYPE).get(FileItem.class);
        assertTrue(file.isFolder());
        client.close();
    }
    */

    /*
    @Test
    public void testBadFilename() {
        Client client = ClientBuilder.newClient();
        FileItem item = new FileItem();
        item.setName("your/badfilename.txt");
        item.setContent("This is a sample content");
        Response response = client.target("http://localhost:8080/fs/api/files").request().post(Entity.entity(item, MediaType.APPLICATION_JSON));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        client.close();
    }
    */

}

