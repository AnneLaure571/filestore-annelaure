package fr.miage.filestore.store.binary;

import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;
import org.wildfly.swarm.undertow.WARArchive;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class BinaryStoreServiceTest {

    private static final Logger LOGGER = Logger.getLogger(BinaryStoreServiceTest.class.getName());

    @Inject
    private BinaryStoreService service;

    @Inject
    @ConfigurationValue("filestore.instance.home")
    private String home;

    @Deployment
    public static Archive createDeployment() throws Exception {
        WARArchive archive = ShrinkWrap.create(WARArchive.class);
        archive.addPackage("fr.miage.filestore.store.binary");
        archive.addAsResource("META-INF/beans.xml");
        archive.addAsResource("project-defaults.yml");
        File[] files =
                Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies()
                        .resolve("junit:junit:4.12").withTransitivity().asFile();
        archive.addAsLibraries(files);

        return archive;
    }

    @After
    public void tearDown() {
        LOGGER.log(Level.INFO, "Need to purge folder: " + home);
    }

    @Test
    public void simpleCreateFileTest() throws BinaryStoreServiceException, BinaryStreamNotFoundException, IOException {
        String content = "This is a test";

        ByteArrayInputStream inputStream = new ByteArrayInputStream(content.getBytes());
        String FILE_ID = service.put(inputStream);
        LOGGER.log(Level.INFO, "File stored with id: " + FILE_ID);
        assertNotNull(FILE_ID);

        assertTrue(service.exists(FILE_ID));

        InputStream inputStream1 = service.get(FILE_ID);

        String retrieved = new String(IOUtils.toByteArray(inputStream1));

        assertEquals(content, retrieved);

        String extract = service.extract(FILE_ID, "test.txt","text/plain" );

        assertEquals("This is a test", extract);
    }


}
