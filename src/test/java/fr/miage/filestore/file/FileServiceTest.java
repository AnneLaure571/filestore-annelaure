package fr.miage.filestore.file;

import fr.miage.filestore.file.entity.FileItem;
import fr.miage.filestore.file.filter.FileFilter;
import org.apache.commons.io.IOUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.undertow.WARArchive;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class FileServiceTest {

    private static final Logger LOGGER = Logger.getLogger(FileServiceTest.class.getName());

    @Inject
    private FileService service;

    @Deployment
    public static Archive createDeployment() throws Exception {
        WARArchive archive = ShrinkWrap.create(WARArchive.class);
        archive.addClass("fr.miage.filestore.store.binary.BinaryStoreService");
        archive.addClass("fr.miage.filestore.store.binary.BinaryStoreServiceException");
        archive.addClass("fr.miage.filestore.store.binary.BinaryStreamNotFoundException");
        archive.addClass("fr.miage.filestore.mock.MockedMemoryBinaryStore");
        archive.addPackage("fr.miage.filestore.config");
        archive.addPackage("fr.miage.filestore.file");
        archive.addPackage("fr.miage.filestore.file.entity");
        archive.addPackage("fr.miage.filestore.file.filter");
        archive.addPackage("fr.miage.filestore.notification");
        archive.addPackage("fr.miage.filestore.notification.entity");
        archive.addPackage("fr.miage.filestore.auth");
        archive.addPackage("fr.miage.filestore.auth.entity");
        archive.addAsResource("META-INF/beans.xml");
        archive.addAsResource("META-INF/persistence.xml");
        archive.addAsResource("project-defaults.yml");
        File[] files =
                Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies()
                        .resolve("junit:junit:4.12").withTransitivity().asFile();
        archive.addAsLibraries(files);

        return archive;
    }

    @Test
    public void createFolderTest() throws FileItemAlreadyExistsException, FileItemNotFoundException, FileServiceException, IOException {
        FileItem root = service.get("");
        assertEquals("root", root.getName());
        assertEquals(0, root.getSize());
        assertEquals(FileItem.FOLDER_MIME_TYPE, root.getMimeType());

        List<FileItem> items = service.list("");
        assertEquals(0, items.size());

        FileItem folder1 = service.add("", "folder1");
        assertEquals(FileItem.FOLDER_MIME_TYPE, folder1.getMimeType());

        items = service.list("");
        assertEquals(1, items.size());
        assertTrue(items.stream().anyMatch(i -> i.getName().equals("folder1")));

        FileItem file1 = service.add("", "file1.txt", new ByteArrayInputStream("This is a sample text file".getBytes()));
        assertEquals("text/plain", file1.getMimeType());
        assertEquals(26, file1.getSize());
        assertEquals("file1.txt", file1.getName());

        items = service.list("");
        assertEquals(2, items.size());
        assertTrue(items.stream().anyMatch(i -> i.getName().equals("folder1")));
        assertTrue(items.stream().anyMatch(i -> i.getName().equals("file1.txt")));

        String contentFile1 = IOUtils.toString(service.getContent(file1.getId()), "UTF-8");
        assertEquals("This is a sample text file", contentFile1);

        FileItem file2 = service.add(folder1.getId(), "file2.txt", new ByteArrayInputStream("This is another sample text file".getBytes()));
        assertEquals("text/plain", file2.getMimeType());
        assertEquals(32, file2.getSize());
        assertEquals("file2.txt", file2.getName());

        String contentFile2 = IOUtils.toString(service.getContent(file2.getId()), "UTF-8");
        assertEquals("This is another sample text file", contentFile2);


        try {
            service.remove("", "folder1");
            fail("this should fail because not empty folder");
        } catch (FileItemNotEmptyException e) {
            //OK
        }

        LOGGER.log(Level.SEVERE, "YOUPLA");
        try {
            service.remove(folder1.getId(), "file2.txt");
            LOGGER.log(Level.SEVERE, "BOUm");
            service.remove("", "folder1");
        } catch (FileItemNotEmptyException e) {
            LOGGER.log(Level.SEVERE, "Problem", e);
            fail("The folder should be empty");
        }


    }


}