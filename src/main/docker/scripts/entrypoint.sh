#!/bin/bash

FS_AUTH_URI=${FS_AUTH_URI:-http://127.0.0.1:8080}
FS_HOME=${FS_HOME:-/opt/jboss/filestore}

cp -a /opt/jboss/config/project-stages-template.yml /opt/jboss/config/project-stages.yml
cp -a /opt/jboss/config/keycloak-template.json /opt/jboss/config/keycloak.json

sed -i "s|##SWARM_BIND_ADDRESS##|$(hostname -i)|" /opt/jboss/config/project-stages.yml
sed -i "s|##FS_HOME##|$FS_HOME|" /opt/jboss/config/project-stages.yml
sed -i "s|##FS_OWNER##|$FS_OWNER|" /opt/jboss/config/project-stages.yml
sed -i "s|##FS_NAME##|$FS_NAME|" /opt/jboss/config/project-stages.yml
sed -i "s|##FS_ID##|$FS_ID|" /opt/jboss/config/project-stages.yml
sed -i "s|##FS_FQDN##|$FS_FQDN|" /opt/jboss/config/project-stages.yml

sed -i "s|##FS_AUTH_URI##|$FS_AUTH_URI|" /opt/jboss/config/keycloak.json

mkdir -p $FS_HOME

java -jar /opt/jboss/store.jar -s /opt/jboss/config/project-stages.yml -S docker
