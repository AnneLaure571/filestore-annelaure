package fr.miage.filestore.file;

import java.io.InputStream;
import java.util.List;

import fr.miage.filestore.file.entity.FileItem;

public interface FileService {

    List<FileItem> list(String id) throws FileServiceException, FileItemNotFoundException;

    List<FileItem> path(String id) throws FileServiceException, FileItemNotFoundException;

    FileItem add(String id, String name) throws FileServiceException, FileItemAlreadyExistsException, FileItemNotFoundException;

    FileItem add(String id, String name, InputStream stream) throws FileServiceException, FileItemAlreadyExistsException, FileItemNotFoundException;

    FileItem get(String id) throws FileServiceException, FileItemNotFoundException;

    InputStream getContent(String id) throws FileServiceException, FileItemNotFoundException;

    void remove(String id, String name) throws FileServiceException, FileItemNotFoundException, FileItemNotEmptyException;

    List<FileItem> search(String query) throws FileServiceException;
    
    List<FileItem> listImages(String id) throws FileItemNotFoundException, FileServiceException;

    List<FileItem> listMusics(String id) throws FileItemNotFoundException, FileServiceException;

    List<FileItem> listVideos(String id) throws FileItemNotFoundException, FileServiceException;
}
