package fr.miage.filestore.auth;

import fr.miage.filestore.auth.entity.Profile;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class AuthenticationServiceBean implements AuthenticationService {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationService.class.getName());

    @Resource
    private SessionContext sessionContext;

    @Inject
    @ConfigurationValue("filestore.instance.owner")
    private String owner;

    private static Map<String, Profile> profilesCache = new HashMap<>();

    @Override
    public boolean isAuthentified() {
        return !sessionContext.getCallerPrincipal().getName().equals(UNAUTHENTIFIED_IDENTIFIER);
    }

    @Override
    public String getConnectedIdentifier() {
        return sessionContext.getCallerPrincipal().getName();
    }

    @Override
    public Profile getConnectedProfile() {
        LOGGER.log(Level.INFO, "Getting connected profile");
        String connectedId = getConnectedIdentifier();
        Profile profile;
        if ( profilesCache.containsKey(connectedId) ) {
            profile = profilesCache.get(connectedId);
        } else {
            profile = new Profile();
            profile.setId(connectedId);
            profilesCache.put(connectedId, profile);
        }
        try {
            KeycloakPrincipal<KeycloakSecurityContext> kcPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) sessionContext.getCallerPrincipal();
            AccessToken token = kcPrincipal.getKeycloakSecurityContext().getToken();
            profile.setUsername(token.getPreferredUsername());
            if ( profile.getUsername().equals(owner) ) {
                LOGGER.log(Level.INFO, "User is owner: " + profile.getUsername());
                profile.setOwner(true);
            } else {
                LOGGER.log(Level.INFO, "User is NOT owner: " + profile.getUsername());
                profile.setOwner(false);
            }
            profile.setFullname(token.getGivenName() + " " + token.getFamilyName());
            profile.setEmail(token.getEmail());
        } catch ( Exception e ) {
            //
        }
        return profile;
    }

}
