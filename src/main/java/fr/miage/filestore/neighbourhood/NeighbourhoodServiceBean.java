package fr.miage.filestore.neighbourhood;

import fr.miage.filestore.neighbourhood.entity.Neighbour;
import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;
import org.wildfly.swarm.topology.AdvertisementHandle;
import org.wildfly.swarm.topology.Topology;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Singleton
@Startup
public class NeighbourhoodServiceBean implements NeighbourhoodService{

    private static final Logger LOGGER = Logger.getLogger(NeighbourhoodService.class.getName());

    @Inject
    @ConfigurationValue("filestore.id")
    private String id;

    @Inject
    @ConfigurationValue("filestore.instance.name")
    private String name;

    private AdvertisementHandle handle;

    @PostConstruct
    public void init() {
        try {
            LOGGER.log(Level.SEVERE, "Advertising topology...");
            handle = Topology.lookup().advertise("filestore-host", "id." + id, "name." + name);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Unable to advertise topology", e);
        }
    }

    @PreDestroy
    public void teardown() {
        if (handle != null) {
            handle.unadvertise();
        }
    }

    @Override
    public List<Neighbour> list() throws NeighbourhoodServiceException {
        try {
            LOGGER.log(Level.INFO, "Listing neighbourhood...");
            Map topology = Topology.lookup().asMap();
            List<Neighbour> neighbours = Collections.emptyList();
            if (topology.containsKey("filestore-host")) {
                neighbours = ((List<Topology.Entry>)topology.get("filestore-host")).stream().map(entry -> {
                    Optional<String> id = entry.getTags().stream().filter(tag -> tag.startsWith("id.")).map(tag -> tag.substring(3)).findFirst();
                    Optional<String> name = entry.getTags().stream().filter(tag -> tag.startsWith("name.")).map(tag -> tag.substring(5)).findFirst();
                    return new Neighbour(id.get(), name.get(), entry.getAddress() + ":" + entry.getPort());
                }).collect(Collectors.toList());
            }
            LOGGER.log(Level.FINE, neighbours.stream().map(Neighbour::toString).collect(Collectors.joining(",")));
            return neighbours;
        } catch (Exception e) {
            throw new NeighbourhoodServiceException("Unable to list neighbourhood", e);
        }
    }

}
