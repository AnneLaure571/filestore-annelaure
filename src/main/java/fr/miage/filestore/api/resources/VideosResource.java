package fr.miage.filestore.api.resources;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import fr.miage.filestore.api.dto.FileUploadForm;
import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.api.template.Template;
import fr.miage.filestore.api.template.TemplateContent;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.file.FileItemAlreadyExistsException;
import fr.miage.filestore.file.FileItemNotFoundException;
import fr.miage.filestore.file.FileService;
import fr.miage.filestore.file.FileServiceException;
import fr.miage.filestore.file.entity.FileItem;

@Path("videos")
@OnlyOwner
public class VideosResource extends FilesResource {

    private static final Logger LOGGER = Logger.getLogger(VideosResource.class.getName());

    @EJB
    private FileService filestore;

    @EJB
    private AuthenticationService auth;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response rootView(@Context UriInfo uriInfo) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/videos (html)");
        FileItem item = filestore.get("");
        URI root = uriInfo.getRequestUriBuilder().path(item.getId()).path("content").build();
        System.out.println(root.toString());
        return Response.seeOther(root).build();
    }

    @GET
    @Path("{id}/content")
    @Template(name = "videos")
    @Produces(MediaType.TEXT_HTML)
    public Response contentView(@PathParam("id") String id, @QueryParam("download") @DefaultValue("false") boolean download) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/videos/" + id + "/content (html)");
        FileItem item = filestore.get(id);
        if ( item.isFolder() ) {
            TemplateContent<Map<String, Object>> content = new TemplateContent<>();
            Map<String, Object> value = new HashMap<>();
            value.put("profile", auth.getConnectedProfile());
            value.put("parent", item);
            value.put("path", filestore.path(item.getId()));
            value.put("items", filestore.listVideos(item.getId()));
            content.setContent(value);
            return Response.ok(content).build();
        } else {
            return Response.ok(filestore.getContent(item.getId()))
                    .header("Content-Type", item.getMimeType())
                    .header("Content-Length", item.getSize())
                    .header("Content-Disposition", ((download) ? "attachment; " : "") + "filename=" + item.getName()).build();
        }
    }
}
