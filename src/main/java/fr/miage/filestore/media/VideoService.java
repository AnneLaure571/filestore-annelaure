package fr.miage.filestore.media;

import java.io.InputStream;
import java.util.List;

import fr.miage.filestore.file.FileItemAlreadyExistsException;
import fr.miage.filestore.file.FileItemNotEmptyException;
import fr.miage.filestore.file.FileItemNotFoundException;
import fr.miage.filestore.file.FileServiceException;
import fr.miage.filestore.file.entity.FileItem;

public interface VideoService {

    List<FileItem> list(String id) throws FileServiceException, FileItemNotFoundException;

    List<FileItem> path(String id) throws FileServiceException, FileItemNotFoundException;

    FileItem add(String id, String name) throws FileServiceException, FileItemAlreadyExistsException, FileItemNotFoundException;

    FileItem add(String id, String name, InputStream stream) throws FileServiceException, FileItemAlreadyExistsException, FileItemNotFoundException;

    FileItem get(String id) throws FileServiceException, FileItemNotFoundException;

    InputStream getContent(String id) throws FileServiceException, FileItemNotFoundException;

    void remove(String id, String name) throws FileServiceException, FileItemNotFoundException, FileItemNotEmptyException;

    List<FileItem> search(String query) throws FileServiceException;

}
